BBackup Readme
==============

BBackup is designed for backing up linux machines using lvm snapshots and tar.
Backup's can contain a mix of lvm snapshotted partitions and normal partitions
(ie /boot is normally just a normal partiton while / is on lvm.
It has 2 types of backup, full and incremental.

Full Backups
------------
Full backups create a new tar archive on every backup which contains everything 
that is chosen to be backed up.

Incremental Backups
-------------------
Incremental backups initially create a full tar backup and a .snap file which
is used by tar to record the differences between backups.
After the first full backup subsequent backups create a new tar file that only
contains the changes since the last backup.

### Restoring Incremental Backups ###
To restore incremental backups you need to extract each tar file in reverse
order using --incremental option.
Eg if there was a full and 2 incrementals restore is done as follows:

	tar --incremental -xf host.example.com_2015-01-01_FULL.tar.bz2
	tar --incremental -xf host.example.com_2015-01-02_INC1.tar.bz2
	tar --incremental -xf host.example.com_2015-01-03_INC2.tar.bz2

Configuration
-------------
Configuration is done in bbackup.conf.rb which lives in the same directory as
bbackup.rb. See bbackup.conf.rb.example for examples.


License
=======
The MIT License (MIT)

Copyright (c) 2015 Todd Bayley <todd@bayley.net.au>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
