#!/usr/bin/env ruby
#
# == Synopsis
# 	BBackup - Linux Backup program that supports lvm snapshots and incremental backups. Outputs std tar files.
#
# == Installation
# 	yum install ruby rubgems
# 	gem install mail # If using old ruby (<1.9) may need to run "gem install mail -v 2.5.4 instead."
#
# == Examples
# 	To configure create a file in same directory as bbackup.rb called bbackup.conf.rb see bbackup.conf.rb.example for details.
#
# == Usage
#	bbackup.rb
#
# == Options
#
# == Author
# 	Todd Bayley todd@bayley.net.au
#
# == Copyright
# 	Copyright (c) 2015 Todd Bayley. Licensed under the MIT License:
# 	http://www.opensource.org/licenses/mit-license.php
#

require 'rubygems'
require 'bigdecimal'
require 'mail'
require 'logger' 
require 'ostruct'
require 'optparse'

#
# Util class to allow logging to multiple locations.
#
class MultiIO
	def initialize(*targets)
		@targets = targets
	end

	def write(*args)
		@targets.each {|t| t.write(*args)}
	end

	def close
		@targets.each(&:close)
	end
end

class App
	VERSION = '0.1'

	attr_accessor :status

	attr_accessor :success
	attr_accessor :backup_path, :backup_file, :incremental_file
	attr_accessor :compression


	
	def initialize(arguments, stdin)
		@arguments = arguments
		@stdin = stdin

		# Set defaults. All options need to be defined here even if only set to nil.
		@options = OpenStruct.new
		@options.lvm_snapshot_size = "500M"
		@options.date = Time.now.strftime("%Y-%m-%d")
		@options.hostname = `hostname`
		@options.mail_to = false
		@options.mail_from =  "backups@#{@options.hostname}"
		@options.retain_days = 7
		@options.incremental = false
		@options.incremental_max = 7
		@options.mount_dir = nil
		@options.exclude = nil
		@options.logs = "/tmp"
		@options.compression = "gzip"
		@options.backups = false
		@options.backup_dest = nil
		@options.force_full = false
		@options.force_incremental = false
		@options.backup_path = nil

	end

	#
	# Main run method of bbackup
	# Parse arguments and set up options before running command.
	#
	def run()
		# Include config file in same directory as this script.
		require File.expand_path(File.dirname(__FILE__) + '/bbackup.conf.rb')
		binaries_extra = []
		# Define Constants
		@options.log_error = "#{@options.logs}/bbackup-tar-error.log"
		@options.log_tar = "#{@options.logs}/bbackup-tar.log"

		# Load settings from config file into @options
		@options.marshal_dump.each do |key,v|
			const = "#{key.to_s.upcase}"
			if(BBackup::Config.const_defined?(const))
				@options.send("#{key}=", BBackup::Config.const_get(const))
			end
		end

		# Parse command line options
		optparse = OptionParser.new do|opts|
			opts.on( '-f', '--full', 'Perform full incremental ignoring max incremental value' ) do
				@options.force_full = true
			end
			opts.on( '-d', '--date DATE', 'Use DATE (YYYY-MM-DD) for backup' ) do |date|
				@options.date = date
			end	
		end
		optparse.parse!
		# Setup logging, Log to both log file and stdout.
		@log_file = File.open("#{@options.logs}/bbackup.log", "w+")
		$LOG = Logger.new(MultiIO.new(STDOUT, @log_file))
		$LOG.formatter = lambda do |severity, datetime, progname, msg|
			# [0..0] is due to ruby 1.8 compatibility issue.
			"#{severity[0..0]}, #{datetime.strftime("%Y-%m-%d,%H:%M:%S")}, #{msg}\n"
		end
		# Default log level is INFO, but can be changed to debug (Logger::DEBUG).
		#$LOG.level = Logger::INFO

		case @options.compression
		when 'gzip'
			@compression = "-z"
			@extension = "tar.gz"			
		when 'bzip2'
			@compression = "-j"
			@extension = "tar.bz2"						
		when 'pigz'
			@compression = "--use-compress-program=pigz"
			@extension = "tar.gz"
			binaries_extra << 'pigz'			
		when 'lbzip2'
			@compression = "--use-compress-program=lbzip2"
			@extension = "tar.bz2"
			binaries_extra << 'lbzip2'
		end

		check_binaries(binaries_extra)
		
		#exit
		backup_start()
	end

	#
	# Run a command returning the exit status.
	#
	def run_cmd(cmd)
		$LOG.debug("RUNCMD: #{cmd}")
		res = `#{cmd}`
		ret = $?.exitstatus
		return ret
	end

	#
	# Returns true if mountpoint is mounted false if not.
	#
	def check_mount(mount)
		res = `mount | grep -q #{mount}`
		ret = $?.exitstatus
		$LOG.debug("Mount query on #{mount} returned #{ret}")
		return ret == 0
	end

	#
	# Returns filessystem type of block device or false if not valid.
	#
	def fs_get_type(device)
		ret = `blkid -o full #{device}`
		match = ret.match(/.*TYPE=\"(.*)\".*/)
		if(match) then
			return match[1]
		else
			$LOG.error("No filesystem Type")
			return false
		end
	end

	#
	# Return latest full backup in a directory or false if does not exist.
	#
	def find_latest_full(directory)
		files = Dir.glob("#{directory}/*_FULL\.#{@extension}")
		newest_date = ""
		latest_full = false
		files.each do |f|
			match = f.match(/.*_(.*)_FULL\.#{@extension}/)
			date = match[1] if match[1]
			if date > newest_date
				latest_full = f
				newest_date = date
			end
		end
		return latest_full
	end

	#
	# Return latest incremental number for a particular full backup.
	#
	def latest_incremental_number(directory, latest_full)
		files = Dir.glob("#{directory}/*_INC*\.#{@extension}")
		if(!latest_full)
			return false
		end
		full_match = latest_full.match(/.*_(.*)_FULL\.#{@extension}/) 
		return 0 if !full_match
		full_date = full_match[1] if full_match[1]
		if(!full_date)
			return false
		end
		latest_inc = 0
		files.each do |f|
			match = f.match(/.*_(.*)_INC(\d+)\.#{@extension}/)
			date = match[1] if match && match[1]
			if(date && date>full_date)
				inc = match[2].to_i if match[2]
				latest_inc = inc if inc > latest_inc
			end
		end
		return latest_inc
	end

	# Check to see if needed binaries are available.
	def check_binaries(extra = [])
		binaries = ['blkid','grep','mount','df','du','tar'] + extra
		binaries.each do|bin|
			`command -v #{bin} 2>/dev/null`
			if($?.exitstatus != 0)
				$LOG.error("Command #{bin} not found")
				exit 2
			end
		end
	end

	#
	# Check if backup has already been performed today.
	# Returns true if it has.
	#
	def check_backup_today(directory, date)
		files = Dir.glob("#{directory}/#{@options.hostname}_#{date}*.#{@extension}")
		return files.length > 0
	end

	#
	# Sets up filenames and basic options.
	#
	# Preliminary incremental tar backup support
	# Initial backup is of format @options.hostname_DATE_FULL.tar.gz (ie test_2015-01-01_FULL.tar.gz
	# Incremental backups after this are @options.hostname_DATE_INC#NUM.tar.gz (ie test_2015-01-01_INC1.tar.gz
	#
	def backup_file_setup()
		if(@options.incremental)
			# Check if backup already taken today
			if (check_backup_today(@options.backup_path, @options.date))
				$LOG.fatal("Backup already taken today #{@options.date}")
				exit 1
			end

			@incremental_base_file = find_latest_full(@options.backup_path)
			$LOG.debug("Latest Full: #{@incremental_base_file}")
			latest_inc = latest_incremental_number(@options.backup_path,@incremental_base_file)
			$LOG.debug("Latest Inc: #{latest_inc}")
			# Check if snapshot data exists and if we are below max incrementals. If so Setup incremental file otherwise do full incremental.
			if(!@options.force_full && (File.exist?("#{@incremental_base_file}.snap") && latest_inc && latest_inc <= @options.incremental_max))
				@incremental_file="#{@incremental_base_file}.snap"
				@backup_file="#{@options.backup_path}/#{@options.hostname}_#{@options.date}_INC#{latest_inc+1}.#{@extension}"
				$LOG.info("INCREMENTAL #{@backup_file} using #{@incremental_file}")
			else
				@backup_file="#{@options.backup_path}/#{@options.hostname}_#{@options.date}_FULL.#{@extension}"
				@incremental_file="#{@backup_file}.snap"
				$LOG.info("FULL INCREMENTAL #{@backup_file} using #{@incremental_file}")
			end
			if(File.exist?(@backup_file))
				$LOG.fatal("Backup file #{@backup_file} already exists")
				exit 1;
			end
			@incremental_options="--listed-incremental #{@incremental_file}"
		else
			# Setup normal backup file
			@backup_file="#{@options.backup_path}/#{@options.hostname}_#{@options.date}_FULL.#{@extension}"
			@incremental_options = ""
		end

	end

	#
	# Start Backup Process
	#
	def backup_start()

		@status = :SUCCESS

		$LOG.info("Starting backup of #{@options.hostname}")

		if(@options.backup_dest[:device] && @options.backup_dest[:mount])
			# Mount backup destination
			mounted = check_mount("#{@options.backup_dest[:mount]}")
			if(mounted)
				$LOG.info("\t#{@options.backup_dest[:mount]} already mounted")
			else
				ret = run_cmd("mount #{@options.backup_dest[:device]} #{@options.backup_dest[:mount]}")
				if(!check_mount("#{@options.backup_dest[:mount]}"))
					$LOG.error("Failed to mount backup destination #{@options.backup_dest[:device]} to #{@options.backup_dest[:mount]}")
					@status = :FAILED
					exit 1
				end
			end
			@options.backup_path = "#{@options.backup_dest[:mount]}/#{@options.backup_dest[:path]}"
			$LOG.info("\tBackup destination #{@options.backup_dest[:mount]} mounted")
		else
			Dir.exist?(@options.backup_dest[:path])
			@options.backup_path = @options.backup_dest[:path]
			$LOG.info("\tUsing backup path #{@options.backup_dest[:path]}")
		end

		# Setup backup filenames
		backup_file_setup()
		
		# Tidy up old backups
		#
		def file_age(name)
		  (Time.now - File.ctime(name))/(24*3600)
		end

		$LOG.info("Cleaning up old backups")

		Dir.glob("#{@options.backup_dest[:mount]}/#{@options.backup_dest[:path]}/#{@options.hostname}*").each do |f|
			$LOG.debug("File: #{f}, age: #{File.ctime(f)}")
			if file_age(f) > @options.retain_days then
				File.delete(f) 
				$LOG.info("Deleting #{f}")
			end

		end

		# Take Snapshots
		$LOG.info("Starting LVM Snapshots")
		@options.backups.each do |back|
			if(back[:type] == 'lvm') then
				$LOG.info("\tLVM Snapshot of #{back[:vg]} #{back[:lv]}")
				run_cmd("lvcreate --size #{@options.lvm_snapshot_size} --snapshot --name #{back[:lv]}-backupsnap /dev/#{back[:vg]}/#{back[:lv]}")
			end
		end

		# Mount Locations
		$LOG.info("Mounting backup sources")
		if(!File.directory?(@options.mount_dir))
			$LOG.info("Creating backup mount dir")
			Dir.mkdir(@options.mount_dir)
		end
		@options.backups.each do |back|
			mount_dir = "#{@options.mount_dir}#{back[:mount]}"
			case back[:type]
			when "lvm"
				options = "-o ro"
				device = "/dev/#{back[:vg]}/#{back[:lv]}-backupsnap"
				Dir.mkdir(mount_dir) unless File.directory?(mount_dir)
				$LOG.info("\tMount LVM #{device} to #{mount_dir}")
				if(fs_get_type(device)=='xfs') then
					options << ",nouuid"
				end
				run_cmd("mount #{options} #{device} #{mount_dir}")
			when "mount"
				$LOG.info("\tMount bind #{back[:mount]} to #{mount_dir}")
				run_cmd("mount --bind #{back[:mount]} #{mount_dir}")
			end
		end

		# Run Backup
		$LOG.info("Running backup")
		excludes = @options.exclude.map { |e| "--exclude=#{e}" }.join(" ")
		$LOG.debug("\tExcludes: #{excludes}")
		ret = run_cmd("tar cvf #{@backup_file} #{@incremental_options} #{excludes} #{@compression} -C #{@options.mount_dir} . > #{@options.log_tar} 2> #{@options.log_error}")
		if(ret != 0)
			$LOG.error("FAILURE: tar failed with error code #{ret}")
			@status = :FAILED
		end

		# Check Backup file created
		if !File.exist?(@backup_file)
			$LOG.error("FAILURE: Backup file not created")
			@status = :FAILED
		end

		# sha256sum backup
		$LOG.info("Calculating sha256 sum")
		run_cmd("sha256sum #{@backup_file} > #{@backup_file}.sha256")


		# Verify Backup
		$LOG.info("Verify backup")
		ret = run_cmd("tar df #{@backup_file} #{@compression} -C #{@options.mount_dir} #{@incremental_options} 2>&1 >> #{@options.log_tar}")
		if(ret != 0)
			$LOG.warn("\tVerify problem with error code #{ret}")
			if(ret == 1)
				@status = :DODGY
			else
				$LOG.warn("\t FAILURE: Verify Failed")		
				@status = :FAILED
			end	
		end

		# Unmount backup soruces
		# Need to reverse so that child mounts are remove first.
		$LOG.info("Unmounting Backup Sources") 
		@options.backups.reverse.each do |back|
			mount_dir = "#{@options.mount_dir}#{back[:mount]}"
			run_cmd("umount #{mount_dir}")
		end

		# Remove Snapshots
		$LOG.info("Removing Snapshots") 
		@options.backups.each do |back|
			if(back[:type] == 'lvm') then
				run_cmd("lvremove -f /dev/#{back[:vg]}/#{back[:lv]}-backupsnap")
			end
		end

		# Calculate Backup Disk Stats
		if @options.backup_dest[:mount]
			ret_df = `df -h|grep #{@options.backup_dest[:mount]}`
			free = ret_df.split(' ')[3]
			used_percent = ret_df.split(' ')[4]
		end
		ret_du = `du -h #{@options.backup_path} --max-depth=1`
		total_used = ret_du.split(' ')[0]
		backup_file_size = File.size(@backup_file)/1024/1024 if File.exist?(@backup_file)

		# Unmount Backup Destination
		if(@options.backup_dest[:device] && @options.backup_dest[:mount])
			$LOG.info("Unmounting backup destination #{@options.backup_dest[:mount]}")
			run_cmd("umount #{@options.backup_dest[:mount]}")
		end

		$LOG.info("Backup Finished as #{@status} (#{@backup_file})")
		$LOG.info("\tSize: #{backup_file_size} MB, Free Space: #{free}, Used: #{used_percent}, Total Used: #{total_used}")

		# Send Backup Report
		if(@options.mail_to) then
			mail = Mail.new
			mail['from'] = @options.mail_from
			mail['to'] = @options.mail_to
			mail['subject'] = "#{@status}: #{@options.hostname} Backup Report"
			
			# Rewind and read log
			@log_file.rewind
			log = @log_file.read
			
			html_part = Mail::Part.new do
				content_type 'text/html; charset=UTF-8'
				body "<html><body><pre>\n"+log+"\n</html></body></pre>"
			end
			mail.html_part = html_part
			mail.delivery_method :sendmail
			mail.deliver
		end
	end
end

# Create and run the application
app = App.new(ARGV, STDIN)
app.run

